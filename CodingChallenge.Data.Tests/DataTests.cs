﻿using System;
using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using NUnit.Framework;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir(new FormaGeometricaList(), Idioma.ListaIdiomas.Castellano));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir(new FormaGeometricaList(), Idioma.ListaIdiomas.Ingles));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new FormaGeometricaList { new Cuadrado(5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perimetro 20 <br/>TOTAL:<br/>1 formas Perimetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new FormaGeometricaList
            {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new FormaGeometricaList
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Idioma.ListaIdiomas.Ingles);

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>TOTAL:<br/>7 shapes Perimeter 97,66 Area 91,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new FormaGeometricaList();
            formas.Add(new Cuadrado(5));
            formas.Add(new Circulo(3));
            formas.Add(new TrianguloEquilatero(4));
            formas.Add(new Cuadrado(2));
            formas.Add(new TrianguloEquilatero(9));
            formas.Add(new Circulo(2.75m));
            formas.Add(new TrianguloEquilatero(4.2m));

            var resumen = FormaGeometrica.Imprimir(formas, Idioma.ListaIdiomas.Castellano);

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perimetro 28 <br/>2 Círculos | Area 13,01 | Perimetro 18,06 <br/>3 Triángulos | Area 49,64 | Perimetro 51,6 <br/>TOTAL:<br/>7 formas Perimetro 97,66 Area 91,65",
                resumen);
        }

        #region Nuevo Idioma
        [TestCase]
        public void TestResumenListaVaciaFormasEnFrances()
        {
            Assert.AreEqual("<h1>Liste vide de formes!</h1>",
                FormaGeometrica.Imprimir(new FormaGeometricaList(), Idioma.ListaIdiomas.Frances));
        }

        [TestCase]
        public void TestResumenListaConUnCuadradoFrances()
        {
            var cuadrados = new FormaGeometricaList { new Cuadrado(5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Frances);

            Assert.AreEqual("<h1>Rapport de formes</h1>1 Carré | Surface 25 | Périmètre 20 <br/>AU TOTAL:<br/>1 formes Périmètre 20 Surface 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadradosEnFrances()
        {
            var cuadrados = new FormaGeometricaList
            {
                new Cuadrado(5),
                new Cuadrado(1),
                new Cuadrado(3)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Frances);

            Assert.AreEqual("<h1>Rapport de formes</h1>3 Carrés | Surface 35 | Périmètre 36 <br/>AU TOTAL:<br/>3 formes Périmètre 36 Surface 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnFrances()
        {
            var formas = new FormaGeometricaList
            {
                new Cuadrado(5),
                new Circulo(3),
                new TrianguloEquilatero(4),
                new Cuadrado(2),
                new TrianguloEquilatero(9),
                new Circulo(2.75m),
                new TrianguloEquilatero(4.2m)
            };

            var resumen = FormaGeometrica.Imprimir(formas, Idioma.ListaIdiomas.Frances);

            Assert.AreEqual(
                "<h1>Rapport de formes</h1>2 Carrés | Surface 29 | Périmètre 28 <br/>2 Cercles | Surface 13,01 | Périmètre 18,06 <br/>3 Triangles | Surface 49,64 | Périmètre 51,6 <br/>AU TOTAL:<br/>7 formes Périmètre 97,66 Surface 91,65",
                resumen);
        }
        #endregion

        #region Trapecio Rectángulo
        [TestCase]
        public void TestResumenListaConUnTrapecio()
        {
            var cuadrados = new FormaGeometricaList { new TrapecioRectangulo( 6, 10, 8, (decimal)6.32 ) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Castellano);

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Trapecio | Area 54 | Perimetro 30,32 <br/>TOTAL:<br/>1 formas Perimetro 30,32 Area 54", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTrapecios()
        {
            var cuadrados = new FormaGeometricaList
            {
                new TrapecioRectangulo(6,10,8),
               new TrapecioRectangulo(3,5,4,(Decimal)3.2),
                new TrapecioRectangulo(6,10,8,(Decimal)6.32)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, Idioma.ListaIdiomas.Ingles);

            Assert.AreEqual("<h1>Shapes report</h1>3 Trapezoids | Area 121,5 | Perimeter 75,84 <br/>TOTAL:<br/>3 shapes Perimeter 75,84 Area 121,5", resumen);
        }
        #endregion
    }
}

﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CodingChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        internal readonly decimal _lado;

        public int Tipo { get; set; }

        public FormaGeometrica(decimal ancho)
        {
            _lado = ancho;
        }

        public static string Imprimir(FormaGeometricaList formas, Idioma.ListaIdiomas idioma)
        {
            Idioma _Idioma = new Idioma(idioma);
            var sb = new StringBuilder();

            if (!formas.Any())
            {
                sb.Append(_Idioma.ListaVacia);
            }
            else
            {
                // Hay por lo menos una forma
                // HEADER
                sb.Append(_Idioma.Reportes);

                var numeroCuadrados = 0;
                var numeroCirculos = 0;
                var numeroTriangulos = 0;
                var numeroTrapecio = 0;

                var areaCuadrados = 0m;
                var areaCirculos = 0m;
                var areaTriangulos = 0m;
                var areaTrapecio = 0m;

                var perimetroCuadrados = 0m;
                var perimetroCirculos = 0m;
                var perimetroTriangulos = 0m;
                var perimitroTrapecio = 0m;

                numeroCuadrados = formas.CantidadForma("Cuadrado");
                areaCuadrados = formas.AreaForma("Cuadrado");
                perimetroCuadrados = formas.PerimetroForma("Cuadrado");
                

                numeroCirculos = formas.CantidadForma("Circulo");
                areaCirculos = formas.AreaForma("Circulo");
                perimetroCirculos = formas.PerimetroForma("Circulo");

                numeroTriangulos = formas.CantidadForma("TrianguloEquilatero");
                areaTriangulos = formas.AreaForma("TrianguloEquilatero");
                perimetroTriangulos = formas.PerimetroForma("TrianguloEquilatero");

                numeroTrapecio = formas.CantidadForma("TrapecioRectangulo");
                areaTrapecio = formas.AreaForma("TrapecioRectangulo");
                perimitroTrapecio = formas.PerimetroForma("TrapecioRectangulo");

                sb.Append(ObtenerLinea(numeroCuadrados, areaCuadrados, perimetroCuadrados, FormaGeometricaList.Formas.Cuadrado, _Idioma));
                sb.Append(ObtenerLinea(numeroCirculos, areaCirculos, perimetroCirculos, FormaGeometricaList.Formas.Circulo, _Idioma));
                sb.Append(ObtenerLinea(numeroTriangulos, areaTriangulos, perimetroTriangulos, FormaGeometricaList.Formas.TrianguloEquilatero, _Idioma));
                sb.Append(ObtenerLinea(numeroTrapecio, areaTrapecio, perimitroTrapecio, FormaGeometricaList.Formas.Trapecio, _Idioma));

                // FOOTER
                sb.Append(_Idioma.Total);
                sb.Append(numeroCuadrados + numeroCirculos + numeroTriangulos + numeroTrapecio +" " + _Idioma.Formas + " ");
                sb.Append(_Idioma.Perimetro + " " + (perimetroCuadrados + perimetroTriangulos + perimetroCirculos + perimitroTrapecio).ToString("#.##") + " ");
                sb.Append(_Idioma.Area + " " + (areaCuadrados + areaCirculos + areaTriangulos + areaTrapecio).ToString("#.##"));
            }

            return sb.ToString();
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, FormaGeometricaList.Formas tipo, Idioma idioma)
        {
            if (cantidad > 0)
            {
                return $"{cantidad} {idioma.TraducirForma(tipo, cantidad)} | {idioma.Area} {area:#.##} | {idioma.Perimetro} {perimetro:#.##} <br/>";
            }

            return string.Empty;
        }

        public abstract decimal CalcularArea();

        public abstract decimal CalcularPerimetro();

    }
    public class FormaGeometricaList : List<FormaGeometrica>
    {
        public FormaGeometricaList()
        {

        }

        public int CantidadForma(String Tipo)
        {
            int _Return = 0;
            foreach (var forma in this)
            {
                if (forma.GetType().Name == Tipo)
                    _Return++;
            }
            return _Return;
        }

        public decimal AreaForma(String Tipo)
        {
            decimal _Return = 0;
            foreach (var forma in this)
            {
                if (forma.GetType().Name == Tipo)
                    _Return += forma.CalcularArea();
            }
            return _Return;
        }

        public decimal PerimetroForma(String Tipo)
        {
            decimal _Return = 0;
            foreach (var forma in this)
            {
                if (forma.GetType().Name == Tipo)
                    _Return += forma.CalcularPerimetro();
            }
            return _Return;
        }

        public enum Formas
        {
            Cuadrado,
            TrianguloEquilatero,
            Circulo,
            Trapecio
        }
    }

    #region Formas Geómetricas
    public class Cuadrado : FormaGeometrica
    {
        public Cuadrado(decimal ancho)
            : base(ancho)
        {

        }

        public override decimal CalcularArea()
        {
            return _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 4;
        }

    }

    public class Circulo : FormaGeometrica
    {
        public Circulo(decimal ancho)
            : base(ancho)
        {

        }

        public override decimal CalcularArea()
        {
            return (decimal)Math.PI * (_lado / 2) * (_lado / 2);
        }

        public override decimal CalcularPerimetro()
        {
            return (decimal)Math.PI * _lado;
        }
    }

    public class TrianguloEquilatero : FormaGeometrica
    {
        public TrianguloEquilatero(decimal ancho)
            : base(ancho)
        {

        }

        public override decimal CalcularArea()
        {
            return ((decimal)Math.Sqrt(3) / 4) * _lado * _lado;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado * 3;
        }
    }

    public class TrapecioRectangulo : FormaGeometrica
    {
        #region Fields
        private decimal _BaseMayor;
        private decimal _BaseMenor;
        private decimal _LadoOblicuo;
        #endregion

        #region Constructors
        public TrapecioRectangulo(decimal ancho, decimal BaseMayor, decimal BaseMenor, decimal LadoOblicuo)
            : base(ancho)
        {
            _BaseMayor = BaseMayor;
            _BaseMenor = BaseMenor;
            _LadoOblicuo = LadoOblicuo;
        }

        public TrapecioRectangulo(decimal ancho, decimal BaseMayor, decimal BaseMenor)
            : base(ancho)
        {
            _BaseMayor = BaseMayor;
            _BaseMenor = BaseMenor;
            var aux = (Double)(_BaseMayor - _BaseMenor);
            _LadoOblicuo = (Decimal) Math.Sqrt(( Math.Pow((Double)_lado,2) + Math.Pow(aux,2) ) );
        }
        #endregion

        #region Methods
        public override decimal CalcularArea()
        {
            return (_lado * (_BaseMayor + _BaseMenor))/2;
        }

        public override decimal CalcularPerimetro()
        {
            return _lado + _LadoOblicuo + _BaseMayor + _BaseMenor;
        }
        #endregion

        #region Properties
        public decimal BaseMayor { get => _BaseMayor; set => _BaseMayor = value; }
        public decimal BaseMenor { get => _BaseMenor; set => _BaseMenor = value; }
        public decimal LadoOblicuo { get => _LadoOblicuo; set => _LadoOblicuo = value; }
        #endregion
    }
    #endregion

    #region Idioma


    public class Idioma
    {
        public enum ListaIdiomas
        {
            Frances,
            Castellano,
            Ingles
        }

        #region Fields
        private String _NombreIdioma = String.Empty;
        private String _ListaVacia = String.Empty;
        private String _Reportes = String.Empty;
        private String _Total = "TOTAL:<br/>";
        private String _Formas = String.Empty;
        private String _Perimetro = String.Empty;
        private String _Area = String.Empty;
        private String _Cuadrado = String.Empty;
        private String _Circulo = String.Empty;
        private String _Triangulo = String.Empty;
        private String _Cuadrados = String.Empty;
        private String _Circulos = String.Empty;
        private String _Triangulos = String.Empty;
        private String _Trapecio = String.Empty;
        private String _Trapecios = String.Empty;
        #endregion

        #region Constructors
        public Idioma(ListaIdiomas Idioma)
        {
            Init(Idioma);
        }

        public string NombreIdioma { get => _NombreIdioma; }
        public string ListaVacia { get => _ListaVacia; }
        public string Reportes { get => _Reportes; }
        public string Total { get => _Total; }
        public string Formas { get => _Formas; }
        public string Perimetro { get => _Perimetro; }
        public string Area { get => _Area; }
        public string Cuadrado { get => _Cuadrado; }
        public string Circulo { get => _Circulo; }
        public string Triangulo { get => _Triangulo; }
        public string Cuadrados { get => _Cuadrados; }
        public string Circulos { get => _Circulos; }
        public string Triangulos { get => _Triangulos; }
        public string Trapecio { get => _Trapecio; }
        public string Trapecios { get => _Trapecios; }
        #endregion

        #region Methods
        private void Init(ListaIdiomas idioma)
        {
            switch (idioma)
            {
                case ListaIdiomas.Castellano:
                    _NombreIdioma = "Castellano";
                    _ListaVacia = "<h1>Lista vacía de formas!</h1>";
                    _Reportes = "<h1>Reporte de Formas</h1>";
                    _Total = "TOTAL:<br/>";
                    _Formas = "formas";
                    _Perimetro = "Perimetro";
                    _Area = "Area";
                    _Cuadrado = "Cuadrado";
                    _Cuadrados = "Cuadrados";
                    _Circulo = "Círculo";
                    _Circulos = "Círculos";
                    _Triangulo = "Triángulo";
                    _Triangulos = "Triángulos";
                    _Trapecio = "Trapecio";
                    _Trapecios = "Trapecios";
                    break;
                case ListaIdiomas.Ingles:
                    _NombreIdioma = "Ingles";
                    _ListaVacia = "<h1>Empty list of shapes!</h1>";
                    _Reportes = "<h1>Shapes report</h1>";
                    _Total = "TOTAL:<br/>";
                    _Formas = "shapes";
                    _Perimetro = "Perimeter";
                    _Area = "Area";
                    _Cuadrado = "Square";
                    _Cuadrados = "Squares";
                    _Circulo = "Circle";
                    _Circulos = "Circles";
                    _Triangulo = "Triangle";
                    _Triangulos = "Triangles";
                    _Trapecio = "Trapezoid";
                    _Trapecios = "Trapezoids";
                    break;
                case ListaIdiomas.Frances:
                    _NombreIdioma = "Frances";
                    _ListaVacia = "<h1>Liste vide de formes!</h1>";
                    _Reportes = "<h1>Rapport de formes</h1>";
                    _Total = "AU TOTAL:<br/>";
                    _Formas = "formes";
                    _Perimetro = "Périmètre";
                    _Area = "Surface";
                    _Cuadrado = "Carré";
                    _Cuadrados = "Carrés";
                    _Circulo = "Cercle";
                    _Circulos = "Cercles";
                    _Triangulo = "Triangle";
                    _Triangulos = "Triangles";
                    _Trapecio = "Trapèze";
                    _Trapecios = "Trapèze";
                    break;
                default:
                    break;
            }
        }

        public string TraducirForma(FormaGeometricaList.Formas Forma, int Cantidad)
        {
            switch (Forma)
            {
                case FormaGeometricaList.Formas.Cuadrado:
                    return Cantidad > 1 ? this.Cuadrados : this.Cuadrado;
                case FormaGeometricaList.Formas.Circulo:
                    return Cantidad > 1 ? this.Circulos : this.Circulo;
                case FormaGeometricaList.Formas.TrianguloEquilatero:
                    return Cantidad > 1 ? this.Triangulos : this.Triangulo;
                case FormaGeometricaList.Formas.Trapecio:
                    return Cantidad > 1 ? this.Trapecios : this.Trapecio;
                default:
                    return "";
            }
        }
        #endregion
    }
    #endregion

}
